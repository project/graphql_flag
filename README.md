# GraphQL Flag

Provides an integration between [GraphQL](https://www.drupal.org/project/graphql)
and [Flag](https://www.drupal.org/project/flag) modules.

## Dependencies

- Flag 8.4.x
- GraphQL 8.3.x

## Features

### Queries

#### Per entity Flaggings

Returns a list of Flaggings for an entity and a Flag.
Example use case: list of users that 'liked' a node.

```graphql
query {
  nodeById(id:"1") {
     entityId
     entityFlagging(flag_id:"like") {
       entityFlaggings {
         flagId
         flagging {
           entityOwner {
             name
             mail
           }
         }
       }
     }
  }
}
```

##### Output

```json
{
  "data": {
    "nodeById": {
      "entityId": "1",
      "entityFlagging": {
        "entityFlaggings": [
          {
            "flagId": "like",
            "flagging": {
              "entityOwner": {
                "name": "John Doe",
                "mail": "john.doe@example.com"
              }
            }
          },
          {
            "flagId": "like",
            "flagging": {
              "entityOwner": {
                "name": "Jane Doe",
                "mail": "jane.doe@example.com"
              }
            }
          }
        ]
      }
    }
  }
}
```

#### Per entity Flagging counts

Returns a list of Flaggings by entity type, for each Flag.
Example use case: number of 'likes' and 'views' for nodes.

##### Query

```graphql
fragment flaggingCounts on EntityFlaggingCountList {
  count: entityFlaggingCounts {
    flagId
    amount
  }
}

query {
  nodeQuery {
    entities {
      entityId
      entityFlaggingCount {
        ... flaggingCounts
      }
    }
  }
}
```

With optional filtering by Flags.
```graphql
query {
  nodeQuery {
    entities {
      entityId
      entityFlaggingCount(flag_id:["like", "view"]) {
        ... flaggingCounts
      }
    }
  }
}
```

##### Output

```json
{
  "data": {
    "nodeQuery": {
      "entities": [
        {
          "entityId": "1",
          "entityFlaggingCount": {
            "count": [
              {
                "flagId": "like",
                "amount": 3
              },
              {
                "flagId": "view",
                "amount": 10
              }
            ]
          }
        },
        {
          "entityId": "2",
          "entityFlaggingCount": {
            "count": [
              {
                "flagId": "like",
                "amount": 0
              },
              {
                "flagId": "view",
                "amount": 5
              }
            ]
          }
        }
      ]
    }
  }
}
```

#### Personal Flaggings

Get personal flaggings on entities for a user.
Applies to the Flags that have the 'Personal' scope.
Example use case: get the 'likes' on entities for a user,
so states for these elements can then be used to flag/unflag
on a decoupled client with mutations queries (see below).

Example, get the user 1 Flaggings for the "like" Flag.

##### Query

```graphql
query {
  userById(id: "1") {
   ...on User {
      entityId
      userFlagging(flag_id: ["like"]) {
        userFlaggings {
          flagId
          entityId
          entityType
        }
      }
    }
  }
}
```

##### Output

```json
{
  "data": {
    "userById": {
      "entityId": "1",
      "userFlagging": {
        "userFlaggings": [
          {
            "flagId": "like",
            "entityId": "1",
            "entityType": "node"
          },
          {
            "flagId": "like",
            "entityId": "2",
            "entityType": "node"
          }
        ]
      }
    }
  }
}
```

### Mutations

There is a generic mutation, with an 'operation' parameter:
'flag' and 'unflag' operations are supported.

Mutation queries are returning errors or the Flagging entity id.

```graphql
mutation($input: FlaggingInput!) {
  flag(input: $input) {
    entity {
      entityId
      entityType
    }
    errors
    violations {
      message
    }
  }
}
```

#### Flag

Example of mutation input for flagging an entity with the 'like' Flag.
The user id is optional, it defaults to the current user id.

```json
{
  "input": {
    "operation": "flag",
    "flag_id":  "like",
    "entity_type": "node",
    "entity_id": 1,
    "user_id": 1
  }
}
```

#### Unflag

Example of mutation input for unflagging an entity with the 'like' Flag.
The user id is optional, it defaults to the current user id.

```json
{
  "input": {
    "operation": "unflag",
    "flag_id":  "like",
    "entity_type": "node",
    "entity_id": 1,
    "user_id": 1
  }
}
```
