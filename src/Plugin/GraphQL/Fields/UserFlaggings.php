<?php

namespace Drupal\graphql_flag\Plugin\GraphQL\Fields;

use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * User flaggings.
 *
 * @GraphQLField(
 *   id = "user_flaggings",
 *   name = "userFlaggings",
 *   description = "User flaggings.",
 *   type = "UserFlagging",
 *   secure = true,
 *   multi = true,
 *   nullable = true,
 *   parents = {
 *     "UserFlaggingList",
 *   },
 * )
 */
class UserFlaggings extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    if (!empty($value['user_flaggings'])) {
      foreach ($value['user_flaggings'] as $userFlagging) {
        yield $userFlagging;
      }
    }
  }

}
