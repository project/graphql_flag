<?php

namespace Drupal\graphql_flag\Plugin\GraphQL\Fields;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\flag\FlagInterface;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use GraphQL\Type\Definition\ResolveInfo;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the flagged entities from a Flag entity id.
 *
 * Limited to the current authenticated user.
 *
 * @GraphQLField(
 *   id = "user_flagging",
 *   name = "userFlagging",
 *   description = @Translation("List of flagged entities from a User."),
 *   secure = true,
 *   type = "UserFlaggingList",
 *   parents = {
 *     "User",
 *   },
 *   arguments = {
 *     "flag_id" = "[String!]"
 *   },
 *   response_cache_max_age = 0,
 * )
 */
class UserFlagging extends FieldPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\flag\FlagServiceInterface definition.
   *
   * @var \Drupal\flag\FlagServiceInterface
   */
  protected $flag;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->flag = $container->get('flag');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    $currentUser = \Drupal::currentUser();
    // @todo add session support for anonymous user.
    // @todo review if some parts could be delegated to the flag service.
    // @todo review user cache context.
    if (!$currentUser->isAnonymous()) {
      $flagIds = $args['flag_id'];
      $userFlags = [];
      foreach ($flagIds as $flagId) {
        $flag = $this->flag->getFlagById($flagId);
        if ($flag instanceof FlagInterface) {
          $query = \Drupal::database()
            ->select('flagging', 'f')
            ->fields('f', ['entity_type', 'entity_id'])
            ->condition('flag_id', $flag->id())
            ->condition('uid', \Drupal::currentUser()->id());
          $queryResult = $query->execute();
          foreach ($queryResult as $userFlag) {
            $userFlags[] = [
              'flagId' => $flag->id(),
              'entityType' => $userFlag->entity_type,
              'entityId' => $userFlag->entity_id,
            ];
          }
        }
      }
      yield [
        'user_flaggings' => $userFlags,
      ];
    }
  }

}
