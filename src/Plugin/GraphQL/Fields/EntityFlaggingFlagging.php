<?php

namespace Drupal\graphql_flag\Plugin\GraphQL\Fields;

use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * Flagging entity related to an entity.
 *
 * @GraphQLField(
 *   id = "entity_flagging_flagging",
 *   name = "flagging",
 *   description = "Flagging entity related to an entity.",
 *   type = "Flagging",
 *   secure = true,
 *   parents = {
 *     "EntityFlagging",
 *   },
 * )
 */
class EntityFlaggingFlagging extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    yield $value['flagging'];
  }

}
