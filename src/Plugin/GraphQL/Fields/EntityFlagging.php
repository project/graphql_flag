<?php

namespace Drupal\graphql_flag\Plugin\GraphQL\Fields;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\flag\FlagInterface;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use GraphQL\Type\Definition\ResolveInfo;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Flagging entities per Flag for an entity.
 *
 * @GraphQLField(
 *   id = "entity_flagging",
 *   name = "entityFlagging",
 *   description = @Translation("Flagging entities per Flag for an entity."),
 *   secure = true,
 *   type = "EntityFlaggingList",
 *   parents = {
 *     "Entity",
 *   },
 *   arguments = {
 *     "flag_id" = "String!",
 *   },
 * )
 */
class EntityFlagging extends FieldPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\flag\FlagServiceInterface definition.
   *
   * @var \Drupal\flag\FlagServiceInterface
   */
  protected $flag;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->flag = $container->get('flag');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    if ($value instanceof FieldableEntityInterface) {
      $flaggings = [];
      $flag = $this->flag->getFlagById(strtolower($args['flag_id']));
      if ($flag instanceof FlagInterface) {
        $entityFlaggings = $this->flag->getEntityFlaggings($flag, $value);
        foreach ($entityFlaggings as $flagging) {
          $flaggings[] = [
            'flagId' => $flag->id(),
            'flagging' => $flagging,
          ];
        }
      }

      yield [
        'entity_flaggings' => $flaggings,
      ];
    }
  }

}
