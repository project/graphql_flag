<?php

namespace Drupal\graphql_flag\Plugin\GraphQL\Fields;

use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * Flagging entity.
 *
 * @GraphQLField(
 *   id = "entity_flaggings",
 *   name = "entityFlaggings",
 *   description = "Flaggings for an entity.",
 *   type = "EntityFlagging",
 *   secure = true,
 *   multi = true,
 *   parents = {
 *     "EntityFlaggingList",
 *   },
 * )
 */
class EntityFlaggings extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    if (!empty($value['entity_flaggings'])) {
      foreach ($value['entity_flaggings'] as $entityFlagging) {
        yield $entityFlagging;
      }
    }
  }

}
