<?php

namespace Drupal\graphql_flag\Plugin\GraphQL\Fields;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\flag\FlagInterface;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use GraphQL\Type\Definition\ResolveInfo;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Amount of flaggings per flag for an entity.
 *
 * Can be used for "like" use cases.
 *
 * @GraphQLField(
 *   id = "entity_flagging_count",
 *   name = "entityFlaggingCount",
 *   description = @Translation("Amount of flaggings per flag for an entity."),
 *   secure = true,
 *   type = "EntityFlaggingCountList",
 *   parents = {
 *     "Entity",
 *   },
 *   arguments = {
 *     "flag_id" = "[String!]"
 *   },
 * )
 */
class EntityFlaggingCount extends FieldPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\flag\FlagServiceInterface definition.
   *
   * @var \Drupal\flag\FlagServiceInterface
   */
  protected $flag;

  /**
   * Drupal\flag\FlagCountManagerInterface definition.
   *
   * @var \Drupal\flag\FlagCountManagerInterface
   */
  protected $flagCount;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->flag = $container->get('flag');
    $instance->flagCount = $container->get('flag.count');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    if ($value instanceof FieldableEntityInterface) {
      $flaggingCounts = [];

      // Filter by Flag id's.
      if (!empty($args['flag_id']) && is_array($args['flag_id'])) {
        foreach ($args['flag_id'] as $flagId) {
          $flag = $this->flag->getFlagById(strtolower($flagId));
          if ($flag instanceof FlagInterface) {
            $counts = $this->flagCount->getEntityFlagCounts($value);
            $flaggingCount = 0;
            if (array_key_exists($flag->id(), $counts)) {
              $flaggingCount = (int) $counts[$flag->id()];
            }
            $flaggingCounts[] = [
              'flagId' => $flag->id(),
              'amount' => $flaggingCount,
            ];
          }
          else {
            // @todo throw error for flags that does not exist
            //   or that does not apply to the entity type / bundle
          }
        }
      }
      // Get all Flags.
      else {
        $counts = $this->flagCount->getEntityFlagCounts($value);
        // The flag count service does not return the flag if it is empty.
        // Make sure to have empty values as well by iterating over the flags.
        $flags = $this->flag->getAllFlags($value->getEntityTypeId(), $value->bundle());
        $flagIds = [];
        foreach ($flags as $flag) {
          if ($flag instanceof FlagInterface) {
            $flagIds[] = $flag->id();
          }
        }
        $flaggingCounts = [];
        $flaggingCountsFlagIds = [];
        // Set initial values from the flag.count service.
        foreach ($counts as $flagId => $count) {
          $flaggingCounts[] = [
            'flagId' => $flagId,
            'amount' => (int) $count,
          ];
          $flaggingCountsFlagIds[] = $flagId;
        }
        // Initialize with flags that have no values.
        $flagIdsToInitialize = array_diff($flagIds, $flaggingCountsFlagIds);
        foreach ($flagIdsToInitialize as $flagId) {
          $flaggingCounts[] = [
            'flagId' => $flagId,
            'amount' => 0,
          ];
        }
      }

      yield [
        'entity_flagging_counts' => $flaggingCounts,
      ];
    }
  }

}
