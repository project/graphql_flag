<?php

namespace Drupal\graphql_flag\Plugin\GraphQL\Fields;

use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * Flag id for entity flagging count.
 *
 * @GraphQLField(
 *   id = "flag_id",
 *   name = "flagId",
 *   description = "Flag id.",
 *   type = "String",
 *   secure = true,
 *   parents = {
 *     "EntityFlaggingCount",
 *     "EntityFlagging",
 *     "UserFlagging",
 *   },
 * )
 */
class FlagId extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    yield $value['flagId'];
  }

}
