<?php

namespace Drupal\graphql_flag\Plugin\GraphQL\Fields;

use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * Entity flagging counts.
 *
 * @GraphQLField(
 *   id = "entity_flagging_counts",
 *   name = "entityFlaggingCounts",
 *   description = @Translation("Entity flagging counts"),
 *   secure = true,
 *   multi = true,
 *   nullable = true,
 *   type = "EntityFlaggingCount",
 *   parents = {
 *     "EntityFlaggingCountList",
 *   },
 * )
 */
class EntityFlaggingCounts extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    if (!empty($value['entity_flagging_counts'])) {
      foreach ($value['entity_flagging_counts'] as $flaggings) {
        yield $flaggings;
      }
    }
  }

}
