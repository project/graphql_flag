<?php

namespace Drupal\graphql_flag\Plugin\GraphQL\Fields;

use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * Amount of flaggings for an entity.
 *
 * @GraphQLField(
 *   id = "entity_flagging_count_amount",
 *   name = "amount",
 *   description = "Amount of flaggings for an entity.",
 *   type = "Integer",
 *   secure = true,
 *   parents = {
 *     "EntityFlaggingCount",
 *   },
 *   response_cache_max_age = 0,
 * )
 */
class EntityFlaggingCountAmount extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    yield $value['amount'];
  }

}
