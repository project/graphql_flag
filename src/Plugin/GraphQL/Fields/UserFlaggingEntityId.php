<?php

namespace Drupal\graphql_flag\Plugin\GraphQL\Fields;

use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * User flagged entity id.
 *
 * @GraphQLField(
 *   id = "user_flagging_entity_id",
 *   name = "entityId",
 *   description = "User flagged entity id.",
 *   type = "Integer",
 *   secure = true,
 *   parents = {
 *     "UserFlagging",
 *   },
 * )
 */
class UserFlaggingEntityId extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    yield $value['entityId'];
  }

}
