<?php

namespace Drupal\graphql_flag\Plugin\GraphQL\Fields;

use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * User flagged entity type.
 *
 * @GraphQLField(
 *   id = "user_flagging_entity_type",
 *   name = "entityType",
 *   description = "User flagged entity type.",
 *   type = "String",
 *   secure = true,
 *   parents = {
 *     "UserFlagging",
 *   },
 * )
 */
class UserFlaggingEntityType extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    yield $value['entityType'];
  }

}
