<?php

namespace Drupal\graphql_flag\Plugin\GraphQL\InputTypes;

use Drupal\graphql\Plugin\GraphQL\InputTypes\InputTypePluginBase;

/**
 * Flagging input type.
 *
 * @GraphQLInputType(
 *   id = "flagging_input",
 *   name = "FlaggingInput",
 *   fields = {
 *     "operation" = {
 *        "type" = "String",
 *        "nullable" = "FALSE"
 *     },
 *     "flag_id" = {
 *        "type" = "String",
 *        "nullable" = "FALSE"
 *     },
 *     "entity_type" = {
 *        "type" = "String",
 *        "nullable" = "FALSE"
 *     },
 *     "entity_id" = {
 *        "type" = "Integer",
 *        "nullable" = "FALSE"
 *     },
 *     "user_id" = {
 *        "type" = "Integer",
 *        "nullable" = "TRUE"
 *     },
 *   }
 * )
 */
class FlaggingInput extends InputTypePluginBase {

}
