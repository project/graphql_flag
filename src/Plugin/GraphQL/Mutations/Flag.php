<?php

namespace Drupal\graphql_flag\Plugin\GraphQL\Mutations;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\flag\FlagInterface;
use Drupal\flag\FlagServiceInterface;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Mutations\MutationPluginBase;
use Drupal\graphql_core\GraphQL\EntityCrudOutputWrapper;
use GraphQL\Type\Definition\ResolveInfo;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;

/**
 * Flag mutation.
 *
 * @GraphQLMutation(
 *   id = "flag",
 *   entity_type = "flagging",
 *   secure = "true",
 *   name = "flag",
 *   type = "EntityCrudOutput!",
 *   arguments = {
 *     "input" = "FlaggingInput"
 *   }
 * )
 */
class Flag extends MutationPluginBase implements ContainerFactoryPluginInterface {
  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Flag service.
   *
   * @var \Drupal\flag\FlagServiceInterface
   */
  protected $flagService;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $pluginId,
    $pluginDefinition,
    EntityTypeManagerInterface $entityTypeManager,
    FlagServiceInterface $flagService,
    AccountProxyInterface $currentUser
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->entityTypeManager = $entityTypeManager;
    $this->flagService = $flagService;
    $this->currentUser = $currentUser;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $pluginDefinition) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('entity_type.manager'),
      $container->get('flag'),
      $container->get('current_user')
    );
  }

  /**
   * Flags the given entity.
   *
   * @param \Drupal\flag\FlagInterface $flag
   *   The flag entity.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to flag.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   (optional) The account of the user flagging the entity. If not given,
   *   the current user is used.
   * @param string $session_id
   *   (optional) The session ID. If $account is NULL and the current user is
   *   anonymous, then this can also be omitted to use the current session.
   *   to identify an anonymous user.
   *
   * @return \Drupal\graphql_core\GraphQL\EntityCrudOutputWrapper
   *   Entity crud output wrapper.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function flag(FlagInterface $flag, EntityInterface $entity, AccountInterface $account = NULL, $session_id = NULL) {
    // The flag() method cannot be reused here as it returns the entity
    // save status and not the entity.
    $bundles = $flag->getBundles();
    $this->flagService->populateFlaggerDefaults($account, $session_id);
    // Check if the entity type corresponds to the flag type.
    if ($flag->getFlaggableEntityTypeId() != $entity->getEntityTypeId()) {
      return new EntityCrudOutputWrapper(
        NULL,
        NULL,
        [$this->t('The flag does not apply to entities of this type.')]
      );
    }
    // Check if the bundle is allowed by the flag.
    if (!empty($bundles) && !in_array($entity->bundle(), $bundles)) {
      return new EntityCrudOutputWrapper(
        NULL,
        NULL,
        [$this->t('The flag does not apply to the bundle of the entity.')]
      );
    }
    // Check whether there is an existing flagging for the combination
    // of flag, entity, and user.
    if ($flag->isFlagged($entity, $account)) {
      return new EntityCrudOutputWrapper(
        NULL,
        NULL,
        [$this->t('The user has already flagged the entity with the flag.')]
      );
    }

    /** @var \Drupal\flag\FlaggingInterface $flaggingEntity */
    $flaggingEntity = $this->entityTypeManager->getStorage('flagging')
      ->create([
        'uid' => $account->id(),
        'session_id' => $session_id,
        'flag_id' => $flag->id(),
        'entity_type' => $entity->getEntityTypeId(),
        'entity_id' => $entity->id(),
    // @todo handle as a parameter.
        'global' => FALSE,
      ]);

    if (($status = $flaggingEntity->save()) && $status === SAVED_NEW) {
      return new EntityCrudOutputWrapper($flaggingEntity);
    }
  }

  /**
   * Unflags the given entity.
   *
   * @param \Drupal\flag\FlagInterface $flag
   *   The flag being unflagged.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to unflag.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   (optional) The account of the user that created the flagging. Defaults
   *   to the current user.
   * @param string $session_id
   *   (optional) If $account is anonymous then the $session_id MUST be
   *   used to identify a user uniquely. If $account and $session_id are NULL
   *   and the current user is anonymous then the current session_id will be
   *   used.
   *
   * @return \Drupal\graphql_core\GraphQL\EntityCrudOutputWrapper
   *   Entity crud output wrapper.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function unflag(FlagInterface $flag, EntityInterface $entity, AccountInterface $account = NULL, $session_id = NULL) {
    $bundles = $flag->getBundles();
    $this->flagService->populateFlaggerDefaults($account, $session_id);
    // Check the entity type corresponds to the flag type.
    if ($flag->getFlaggableEntityTypeId() != $entity->getEntityTypeId()) {
      return new EntityCrudOutputWrapper(
        NULL,
        NULL,
        [$this->t('The flag does not apply to entities of this type.')]
      );
    }
    // Check if the bundle is allowed by the flag.
    if (!empty($bundles) && !in_array($entity->bundle(), $bundles)) {
      return new EntityCrudOutputWrapper(
        NULL,
        NULL,
        [$this->t('The flag does not apply to the bundle of the entity.')]
      );
    }
    /** @var \Drupal\flag\FlaggingInterface $flaggingEntity */
    $flaggingEntity = $this->flagService->getFlagging($flag, $entity, $account, $session_id);
    // Check whether there is an existing flagging for the combination of flag,
    // entity, and user.
    if (!$flaggingEntity) {
      return new EntityCrudOutputWrapper(
        NULL,
        NULL,
        [$this->t('The entity is not flagged by the user.')]
      );
    }

    $flaggingEntity->delete();
    return new EntityCrudOutputWrapper($flaggingEntity);
  }

  /**
   * Checks if the Flag operation is supported.
   *
   * @param string $operation
   *   Operation.
   *
   * @return bool
   *   Flag based supported operation.
   */
  private function isSupportedOperation($operation) {
    return in_array($operation, ['flag', 'unflag']);
  }

  /**
   * {@inheritdoc}
   */
  public function resolve($value, array $args, ResolveContext $context, ResolveInfo $info) {
    $operation = (string) $args['input']['operation'];
    $flagId = (string) $args['input']['flag_id'];
    $entityId = (int) $args['input']['entity_id'];
    $entityType = (string) $args['input']['entity_type'];
    $userId =
      array_key_exists('user_id', $args['input']) && !empty($args['input']['user_id']) ?
        (int) $args['input']['user_id'] : $this->currentUser->id();
    // @todo session for anonymous users.
    $sessionId = NULL;

    if (!$this->isSupportedOperation($operation)) {
      return new EntityCrudOutputWrapper(
        NULL,
        NULL,
        [$this->t('The @operation operation is not supported.', [
          '@operation' => $operation,
        ]),
        ]
      );
    }

    try {
      /** @var \Drupal\flag\FlagInterface $flag */
      $flag = $this->entityTypeManager->getStorage('flag')->load($flagId);
      /** @var \Drupal\Core\Entity\EntityInterface $entity */
      $entity = $this->entityTypeManager->getStorage($entityType)->load($entityId);
      /** @var \Drupal\user\UserInterface $account */
      $account = $this->entityTypeManager->getStorage('user')->load($userId);

      // Introducing some of the FlagService logic here so errors can be
      // returned back. They could be implemented as constraints by the
      // Flag module so most of this logic could be shared then returned
      // as violations.
      switch ($operation) {
        case 'flag':
          return $this->flag($flag, $entity, $account);

        case 'unflag':
          return $this->unflag($flag, $entity, $account);
      }
    }
    catch (\Exception $exception) {
      return new EntityCrudOutputWrapper(
        NULL,
        NULL,
        [$exception->getMessage()]
          );
    }
  }

}
