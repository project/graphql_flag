<?php

namespace Drupal\graphql_flag\Plugin\GraphQL\Types;

use Drupal\graphql\Plugin\GraphQL\Types\TypePluginBase;

/**
 * User flagging type.
 *
 * @GraphQLType(
 *   id = "user_flagging",
 *   name = "UserFlagging",
 * )
 */
class UserFlagging extends TypePluginBase {

}
