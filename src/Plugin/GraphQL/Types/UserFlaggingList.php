<?php

namespace Drupal\graphql_flag\Plugin\GraphQL\Types;

use Drupal\graphql\Plugin\GraphQL\Types\TypePluginBase;

/**
 * User flagging list type.
 *
 * @GraphQLType(
 *   id = "user_flagging_list",
 *   name = "UserFlaggingList",
 * )
 */
class UserFlaggingList extends TypePluginBase {

}
