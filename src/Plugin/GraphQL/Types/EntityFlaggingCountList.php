<?php

namespace Drupal\graphql_flag\Plugin\GraphQL\Types;

use Drupal\graphql\Plugin\GraphQL\Types\TypePluginBase;

/**
 * User flagging count list type.
 *
 * @GraphQLType(
 *   id = "entity_flagging_count_list",
 *   name = "EntityFlaggingCountList",
 * )
 */
class EntityFlaggingCountList extends TypePluginBase {

}
