<?php

namespace Drupal\graphql_flag\Plugin\GraphQL\Types;

use Drupal\graphql\Plugin\GraphQL\Types\TypePluginBase;

/**
 * User flagging count type.
 *
 * @GraphQLType(
 *   id = "entity_flagging_count",
 *   name = "EntityFlaggingCount",
 * )
 */
class EntityFlaggingCount extends TypePluginBase {

}
