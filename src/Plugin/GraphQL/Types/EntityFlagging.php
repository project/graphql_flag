<?php

namespace Drupal\graphql_flag\Plugin\GraphQL\Types;

use Drupal\graphql\Plugin\GraphQL\Types\TypePluginBase;

/**
 * Entity flagging type.
 *
 * @GraphQLType(
 *   id = "entity_flagging",
 *   name = "EntityFlagging",
 * )
 */
class EntityFlagging extends TypePluginBase {

}
