<?php

namespace Drupal\graphql_flag\Plugin\GraphQL\Types;

use Drupal\graphql\Plugin\GraphQL\Types\TypePluginBase;

/**
 * Entity flagging list type.
 *
 * @GraphQLType(
 *   id = "entity_flagging_list",
 *   name = "EntityFlaggingList",
 * )
 */
class EntityFlaggingList extends TypePluginBase {

}
