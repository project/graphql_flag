<?php

namespace Drupal\Tests\graphql_flag\Kernel;

/**
 * Tests GraphQL personal Flag.
 *
 * @group graphql_flag
 */
class GraphQLPersonalFlagTest extends GraphQLFlagTestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->flag = $this->flagCreatePersonalFlag('node', ['page']);
  }

  /**
   * Returns the Flag mutation input.
   *
   * @param string $operation
   *   The Flag operation: 'flag' or 'unflag'.
   *
   * @return array
   *   The mutation input variables.
   */
  private function getMutationInput($operation) {
    return [
      'input' => [
        'operation' => $operation,
        'flag_id' => $this->flag->id(),
        'entity_type' => $this->node->getEntityTypeId(),
        'entity_id' => $this->node->id(),
        'user_id' => $this->adminUser->id(),
      ],
    ];
  }

  /**
   * Tests the Flag mutation result for flagging / unflagging.
   */
  public function testFlagEntity() {
    $query = $this->getQueryFromFile('flag.graphql');
    // Flag the entity.
    $result = $this->processQuery($query, $this->getMutationInput('flag'))->toArray();
    $this->assertEmpty($result['data']['flag']['errors']);
    // Flag the entity a second time.
    $result = $this->processQuery($query, $this->getMutationInput('flag'))->toArray();
    $this->assertEqual($result['data']['flag']['errors'][0], 'The user has already flagged the entity with the flag.');
    // Unflag the entity.
    $result = $this->processQuery($query, $this->getMutationInput('unflag'))->toArray();
    $this->assertEmpty($result['data']['flag']['errors']);
    // Unflag the entity a second time.
    $result = $this->processQuery($query, $this->getMutationInput('unflag'))->toArray();
    $this->assertEqual($result['data']['flag']['errors'][0], 'The entity is not flagged by the user.');
  }

}
