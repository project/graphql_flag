<?php

namespace Drupal\Tests\graphql_flag\Kernel;

use Drupal\Tests\graphql\Kernel\GraphQLTestBase;
use Drupal\Tests\node\Traits\ContentTypeCreationTrait;
use Drupal\Tests\user\Traits\UserCreationTrait;
use Drupal\flag\Tests\FlagCreateTrait;
use Drupal\node\Entity\Node;
use GraphQL\Server\OperationParams;

/**
 * Base class for GraphQL Flag tests.
 *
 * @group graphql_flag
 */
abstract class GraphQLFlagTestBase extends GraphQLTestBase {

  use UserCreationTrait {
    createUser as drupalCreateUser;
    createRole as drupalCreateRole;
    createAdminRole as drupalCreateAdminRole;
  }
  use ContentTypeCreationTrait {
    createContentType as drupalCreateContentType;
  }
  use FlagCreateTrait {
    createFlag as flagCreatePersonalFlag;
    createGlobalFlag as flagCreateGlobalFlag;
  }

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'system',
    'field',
    'filter',
    'text',
    'user',
    'node',
    'language',
    'graphql_core',
    'flag',
    'graphql_flag',
  ];

  /**
   * The Node entity that is the subject of the Flag.
   *
   * @var \Drupal\node\Entity\Node
   */
  protected $node;

  /**
   * The Flag entity, that can be personal or global.
   *
   * @var \Drupal\flag\Entity\Flag
   */
  protected $flag;

  /**
   * A User entity with all permissions.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->installEntitySchema('user');
    $this->installEntitySchema('node');
    $this->installEntitySchema('flagging');
    $this->installSchema('system', ['sequences']);
    $this->installSchema('flag', ['flag_counts']);
    $this->installSchema('node', ['node_access']);
    $this->installConfig(['filter', 'flag', 'node']);

    // Create user 1 to abstract permissions.
    $this->adminUser = $this->drupalCreateUser();

    // Create a page Node type.
    $this->drupalCreateContentType([
      'type' => 'page',
      'name' => 'Basic page',
      'display_submitted' => FALSE,
    ]);

    // Create a Node for the Flag.
    $this->node = $this->createNode();
  }

  /**
   * Creates a page Node with minimal settings.
   */
  protected function createNode() {
    $node = Node::create([
      'type' => 'page',
      'title' => $this->randomString(10),
    ]);
    $node->save();
    return $node;
  }

  /**
   * Processes a GraphQL query or mutation.
   *
   * @param string $query
   *   The GraphQL query.
   * @param array $variables
   *   Optional query parameters.
   *
   * @return \Drupal\graphql\GraphQL\Execution\QueryResult|\Drupal\graphql\GraphQL\Execution\QueryResult[]
   *   The query result.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function processQuery($query, array $variables = []) {
    return $this->graphQlProcessor()->processQuery(
      $this->getDefaultSchema(),
      OperationParams::create([
        'query' => $query,
        'variables' => $variables,
      ])
    );
  }

}
